#ifndef XAL_LIBS_MATH_HPP
#define XAL_LIBS_MATH_HPP

#include <array>
#include <cmath>
#include <cassert>
#include <limits>
#include <iostream>
#include "iterator_tpl.h"

#undef near
#undef far

// ================================================================= //
//                        General Functions
// ================================================================= //

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028
#endif

/// Clamp a value to a given range
template<class T> inline T clamp(const T& v, const T& min, const T& max) {
    return v < min ? min : v > max ? max : v;
}

/// Invert avoiding division by zero
inline float invert(float f) {
    return f != 0.0f ? 1.0f / f : 0.0f;
}

/// Floating point modulus
template<class T> inline T fmod(T a, T b) {
    return ((a) - ((int)((a)/(b))) * (b));
}

/// Mix two values by the given factor
template<class T> inline T mix(T a, T b, T factor) {
    return a + (factor * (b - a));
}

/// Wrap a value between 0 and max
template<class T> inline T wrap_max(const T& v, const T& max) {
    return fmod(max + fmod(v, max), max);
}

inline int wrap_max(const int v, const int max) { return (max + (v % max)) % max; }
inline unsigned wrap_max(const unsigned v, const unsigned max) { return (max + (v % max)) % max; }

/// Wrap a value in the range [min, max)
template<class T> inline T wrap(const T& v, const T& min, const T& max) {
    return min + wrap_max(v - min, max - min);
}

/// Convert an angle in degrees to radians
template<class T> inline T deg2rad(const T& v) {
    return v * (T) M_PI / 180;
}

/// Convert an angle in radians to degrees
template<class T> inline T rad2deg(const T& v) {
    return v * 180 / (T) M_PI;
}

/// Relative floating point comparison
template<typename T> inline bool relative_eq(T f1, T f2) {
    auto max = std::fmax(10, std::fmax(fabs(f1), fabs(f2)));
    auto e = std::numeric_limits<T>::epsilon();
    return std::fabs(f1 - f2) <= e * max;
}

template<> inline bool relative_eq<int>(int f1, int f2) {
    return f1 == f2;
}


// ================================================================= //
//                            Vectors
// ================================================================= //

template<class S> struct v2_t;
template<class S> struct v3_t;
template<class S> struct v4_t;

/// A 2D Vector
template<class S> struct v2_t {
    S x = 0, y = 0;

    inline v2_t() = default;
    explicit inline v2_t(S a): x(a), y(a) {}
    explicit inline v2_t(S x, S y): x(x), y(y) {}

    /// Cast to a different vector type.
    template<class T> inline v2_t<T> cast() const {
        return v2_t<T>{ static_cast<T>(x), static_cast<T>(y) };
    }

    /// The dot product of two vectors.
    inline S dot(const v2_t<S>& rhs) const {
        return (x * rhs.x) + (y * rhs.y);
    }

    /// The squared magnitude of the vector
    inline S length_sq() const {
        return dot(*this);
    }

    /// The magnitude of the vector
    inline S length() const {
        return std::sqrt(length_sq());
    }

    /// Get the angle of the vector from the positive x axis in degrees.
    inline float angle() const {
        return rad2deg(std::atan2(y, x));
    }

    /// Get a unit vector pointing in the same direction
    inline v2_t<S> normalize() const {
        auto len = length();
        if (len == 0) return v2_t<S>();
        else return v2_t<S>{x / len, y / len};
    }

    /// Get the tangent to this vector
    inline v2_t<S> perpendicular() const {
        return v2_t<S>{ -y, x };
    }

    /// Test if two vectors are perpendicular
    inline bool is_perpendicular(v2_t<S> b) {
        return relative_eq(dot(b), 0);
    }

    /// Compute the angle to another vector in radians
    inline S angle(v2_t<S> b) {
        return std::acos(dot(b) / (length() * b.length()));
    }

    /// The cross product (perpendicular dot product) of two vectors.
    inline S perp_dot(const v2_t<S> &rhs) const {
        return (x * rhs.y) - (y * rhs.x);
    }

    inline v2_t<S> operator-() const { return v2_t<S>{ -x, -y }; }

    inline v2_t<S> operator+(S v) const { return v2_t<S>{x + v, y + v}; }
    inline v2_t<S> operator-(S v) const { return v2_t<S>{x - v, y - v}; }
    inline v2_t<S> operator*(S v) const { return v2_t<S>{x * v, y * v}; }
    inline v2_t<S> operator/(S v) const { return v2_t<S>{x / v, y / v}; }

    inline v2_t<S> operator+(const v2_t<S>& rhs) const { return v2_t<S>{x + rhs.x, y + rhs.y}; }
    inline v2_t<S> operator-(const v2_t<S>& rhs) const { return v2_t<S>{x - rhs.x, y - rhs.y}; }
    inline v2_t<S> operator*(const v2_t<S>& rhs) const { return v2_t<S>{x * rhs.x, y * rhs.y}; }
    inline v2_t<S> operator/(const v2_t<S>& rhs) const { return v2_t<S>{x / rhs.x, y / rhs.y}; }

    inline bool operator==(const v2_t<S>& rhs) const {
        return relative_eq(x, rhs.x) && relative_eq(y, rhs.y);
    }

    inline bool operator!=(const v2_t &rhs) const {
        return !(rhs == *this);
    }
};

template <class T> std::ostream& operator<<(std::ostream& os, const v2_t<T>& v)  {
    os << "(" << v.x << "," << v.y << ")";
    return os;
}

template<class S> inline bool relative_eq(v2_t<S> a, v2_t<S> b) {
    return relative_eq(a.x, b.x) && relative_eq(a.y, b.y);
}

using v2  = v2_t<float>;
using v2i = v2_t<int32_t>;
using v2u = v2_t<uint32_t>;

/// A 3D Vector
template<class S> struct v3_t {
    union {
        struct {
            S x;
            S y;
            S z;
        };
        float v[3];
    };

    inline S& operator[](size_t idx) { return v[idx]; }

    inline S const& operator[](size_t idx) const { return v[idx]; }

    inline v3_t(S x, S y, S z): x(x), y(y), z(z) {}
    explicit inline v3_t(S a): v3_t(a, a, a) {}
    inline v3_t(): v3_t(0) {}
    explicit inline v3_t(v4_t<S> a);

    /// Cast to a different vector type.
    template<class T> inline v3_t<T> cast() const {
        return v3_t<T>{ static_cast<T>(x), static_cast<T>(y), static_cast<T>(z) };
    }

    /// Compute the cross product of two vectors.
    inline v3_t<S> cross(v3_t<S> const &b) const {
        auto a = *this;
        return v3_t<S> {
                b.z * a.y - b.y * a.z,
                b.x * a.z - b.z * a.x,
                b.y * a.x - b.x * a.y,
        };
    }

    /// Compute the dot product (inner product) of two vectors.
    inline S dot(v3_t<S> const& b) const {
        auto a = *this;
        return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
    }

    /// Test if two vectors are perpendicular
    inline bool is_perpendicular(v3_t<S> b) {
        return relative_eq(dot(b), 0);
    }

    /// Compute the angle to another vector in radians
    inline S angle(v3_t<S> b) {
        return std::acos(dot(b) / (length() * b.length()));
    }

    /// The squared magnitude of the vector
    inline S length_sq() const {
        return dot(*this);
    }

    /// The magnitude of the vector
    inline S length() const {
        return std::sqrt(length_sq());
    }

    /// Get a unit vector pointing in the same direction
    inline v3_t<S> normalize() const {
        auto len = length();
        if (len == 0) return v3_t<S>();
        else return v3_t<S>{ x / len, y / len, z / len };
    }

    inline bool is_valid() const {
        return std::isfinite(x) && std::isfinite(y) && std::isfinite(z) &&
               abs(x) < 1'000'000'000.f && abs(y) < 1'000'000'000.f && abs(z) < 1'000'000'000.f;
    }

    inline v3_t<S> operator+(S v) const { return v3_t<S>{ x + v, y + v, z + v }; }
    inline v3_t<S> operator-(S v) const { return v3_t<S>{ x - v, y - v, z - v }; }
    inline v3_t<S> operator*(S v) const { return v3_t<S>{ x * v, y * v, z * v }; }
    inline v3_t<S> operator/(S v) const { return v3_t<S>{ x / v, y / v, z / v }; }

    inline v3_t<S> operator+(v3_t<S> v) const { return v3_t{ x + v.x, y + v.y, z + v.z }; }
    inline v3_t<S> operator-(v3_t<S> v) const { return v3_t{ x - v.x, y - v.y, z - v.z }; }
    inline v3_t<S> operator*(v3_t<S> v) const { return v3_t{ x * v.x, y * v.y, z * v.z }; }
    inline v3_t<S> operator/(v3_t<S> v) const { return v3_t{ x / v.x, y / v.y, z / v.z }; }

    inline v3_t<S> operator-() const { return v3_t{ -x, -y, -z }; }

    inline bool operator==(const v3_t &rhs) const {
        return relative_eq(x, rhs.x) && relative_eq(y, rhs.y) && relative_eq(z, rhs.z);
    }

    inline bool operator!=(const v3_t &rhs) const {
        return !(rhs == *this);
    }
};

template <class T> std::ostream& operator<<(std::ostream& os, const v3_t<T>& v)  {
    os << "(" << v.x << "," << v.y << "," << v.z << ")";
    return os;
}

template<class S> inline bool relative_eq(v3_t<S> a, v3_t<S> b) {
    return relative_eq(a.x, b.x) && relative_eq(a.y, b.y) && relative_eq(a.z, b.z);
}

template<class T> inline v3_t<T> mix(v3_t<T> a, v3_t<T> b, T factor) {
    return a + ((b - a) * factor);
}

using v3  = v3_t<float>;
using v3i = v3_t<int32_t>;
using v3u = v3_t<uint32_t>;

inline std::array<v3, 2> compute_basis(v3 a) {
    // http://box2d.org/2014/02/computing-a-basis/
    v3 b{};
    if (std::abs(a.x) >= 0.57735027) b = v3{ a.y, -a.x, 0.0 };
    else b = v3{ 0.0, a.z, -a.y };
    b = b.normalize();
    return { b, a.cross(b) };
}

/// A 4D Vector
template<class S> struct v4_t {
    union{ S x = 0; S r; };
    union{ S y = 0; S g; };
    union{ S z = 0; S b; };
    union{ S w = 0; S a; };

    inline v4_t() = default;
    explicit inline v4_t(S a): x(a), y(a), z(a), w(a) {}
    explicit inline v4_t(S x, S y, S z, S w): x(x), y(y), z(z), w(w) {}
    explicit inline v4_t(v3_t<S> v, S w): v4_t(v.x, v.y, v.z, w) {}
    inline v4_t(S const v[4]): v4_t(v[0], v[1], v[2], v[3]) {}

    /// Compute the dot product (inner product) of two vectors.
    inline S dot(v4_t<S> b) const {
        auto a = *this;
        return (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w);
    }

    /// Test if two vectors are perpendicular
    inline bool is_perpendicular(v4_t<S> b) {
        return relative_eq(dot(b), 0);
    }

    /// Compute the angle to another vector in radians
    inline S angle(v4_t<S> b) {
        return std::acos(dot(b) / (length() * b.length()));
    }

    /// The squared magnitude of the vector
    inline S length_sq() const {
        return dot(*this);
    }

    /// The magnitude of the vector
    inline S length() const {
        return std::sqrt(length_sq());
    }

    /// Get a unit vector pointing in the same direction
    inline v4_t<S> normalize() const {
        auto len = length();
        if (len == 0) return v4_t<S>();
        else return v4_t<S>{ x / len, y / len, z / len, w / len };
    }

    inline v2 xy() { return v2_t<S>(x, y); }

    inline v4_t<S> operator+(S v) const { return v4_t{ x + v, y + v, z + v, w + v }; }
    inline v4_t<S> operator-(S v) const { return v4_t{ x - v, y - v, z - v, w - v }; }
    inline v4_t<S> operator*(S v) const { return v4_t{ x * v, y * v, z * v, w * v }; }
    inline v4_t<S> operator/(S v) const { return v4_t{ x / v, y / v, z / v, w / v }; }

    inline v4_t<S> operator+(v4_t<S> v) const { return v4_t{ x + v.x, y + v.y, z + v.z, w + v.w }; }
    inline v4_t<S> operator-(v4_t<S> v) const { return v4_t{ x - v.x, y - v.y, z - v.z, w - v.w }; }
    inline v4_t<S> operator*(v4_t<S> v) const { return v4_t{ x * v.x, y * v.y, z * v.z, w * v.w }; }
    inline v4_t<S> operator/(v4_t<S> v) const { return v4_t{ x / v.x, y / v.y, z / v.z, w / v.w }; }

    inline v4_t<S> operator-() const { return v4_t{ -x, -y, -z, -w }; }

    bool operator==(const v4_t &rhs) const {
        return relative_eq(x, rhs.x) && relative_eq(y, rhs.y) && relative_eq(z, rhs.z) && relative_eq(w, rhs.w);
    }

    bool operator!=(const v4_t &rhs) const {
        return !(rhs == *this);
    }
};

/// A 4D float vector.
using v4 = v4_t<float>;

template <class T> std::ostream& operator<<(std::ostream& os, const v4_t<T>& v)  {
    os << "(" << v.x << "," << v.y << "," << v.z << "," << v.w, ")";
    return os;
}

template<class T> inline v4_t<T> mix(v4_t<T> a, v4_t<T> b, T factor) {
    return a + ((b - a) * factor);
}

template<class S>
inline v3_t<S>::v3_t(v4_t<S> a): x(a.x), y(a.y), z(a.z) { }

// ================================================================= //
//                            Matrix
// ================================================================= //

/// A 3x3 matrix
template<class T> struct m3_t {
    union {
        struct {
            T m00, m01, m02;
            T m10, m11, m12;
            T m20, m21, m22;
        };
        T m[3][3];
    };

    inline explicit m3_t(v3_t<T> a, v3_t<T> b, v3_t<T> c):
            m00(a.x), m01(a.y), m02(a.z),
            m10(b.x), m11(b.y), m12(b.z),
            m20(c.x), m21(c.y), m22(c.z) {}

    inline explicit m3_t(T v):
        m { {v, 0, 0}, {0, v, 0}, {0, 0, v} } {}

    /// The identity matrix.
    inline static m3_t<T> id() {
        return m3_t {
            v3{1, 0, 0},
            v3{0, 1, 0},
            v3{0, 0, 1},
        };
    }

    /// Calculate the determinant
    inline T determinant() const {
        return m00 * (m11 * m22 - m12 * m21) + m10 * (m21 * m02 - m01 * m22) + m20 * (m01 * m12 - m11 * m02);
    }

    /// Transpose the matrix
    inline m3_t<T> transpose() const {
        return m3_t {
            v3_t { m00, m10, m20 },
            v3_t { m01, m11, m21 },
            v3_t { m02, m12, m22 },
        };
    }

    /// Get the matrix's inverse
    inline m3_t<T> invert() const {
        if (determinant() == 0.0) throw std::logic_error("Matrix::invert on non-invertible matrix");
        T x = 1 / determinant();
        return m3_t {
            v3_t { x * (m11 * m22 - m12 * m21), x * (m21 * m02 - m01 * m22), x * (m01 * m12 - m11 * m02) },
            v3_t { x * (m20 * m12 - m10 * m22), x * (m00 * m22 - m20 * m02), x * (m10 * m02 - m00 * m12) },
            v3_t { x * (m10 * m21 - m20 * m11), x * (m20 * m01 - m00 * m21), x * (m00 * m11 - m10 * m01) },
        };
    }

    inline m3_t<T> operator*(const m3_t<T>& a) const {
        auto b = *this;
        return m3_t {
            v3_t { a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20, a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21, a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22 },
            v3_t { a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20, a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21, a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22 },
            v3_t { a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20, a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21, a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22 },
        };
    }

    inline v2_t<T> operator*(const v2_t<T>& v) const {
        return v2_t<T> {
            m00 * v.x + m10 * v.y + m20,
            m01 * v.x + m11 * v.y + m21,
        };
    }

    inline m3_t<T> operator*(T v) const {
        return m3_t<T> {
                v3_t { m00 * v, m10 * v, m20 * v },
                v3_t { m01 * v, m11 * v, m21 * v },
                v3_t { m02 * v, m12 * v, m22 * v },
        };
    }

    inline v3_t<T> operator*(v3_t<T> v) const {
        return v3_t<T> {
            m00 * v.x + m10 * v.y + m20 * v.z,
            m01 * v.x + m11 * v.y + m21 * v.z,
            m02 * v.x + m12 * v.y + m22 * v.z,
        };
    }
};

using m3 = m3_t<float>;

template <class T> std::ostream& operator<<(std::ostream& os, const m3_t<T>& m)  {
    os << "|" << m.m00 << ", " << m.m01 << ", " << m.m02 << "|" << std::endl;
    os << "|" << m.m10 << ", " << m.m11 << ", " << m.m12 << "|" << std::endl;
    os << "|" << m.m20 << ", " << m.m21 << ", " << m.m22 << "|";
    return os;
}

/// A 4x4 matrix
template<class T> struct m4_t {
    union {
        struct {
            T m00, m01, m02, m03;
            T m10, m11, m12, m13;
            T m20, m21, m22, m23;
            T m30, m31, m32, m33;
        };
        T m[4][4];
    };

    inline T& operator[](size_t idx) { return m[idx / 4][idx % 4]; }
    inline T const& operator[](size_t idx) const { return m[idx / 4][idx % 4]; }

    inline m4_t& operator=(m4_t const& rhs) = default;

    inline m4_t() = default;
    inline explicit m4_t(v4_t<T> a, v4_t<T> b, v4_t<T> c, v4_t<T> d):
            m00(a.x), m01(a.y), m02(a.z), m03(a.w),
            m10(b.x), m11(b.y), m12(b.z), m13(b.w),
            m20(c.x), m21(c.y), m22(c.z), m23(c.w),
            m30(d.x), m31(d.y), m32(d.z), m33(d.w) {}

     inline explicit m4_t(T v):
            m { {v, 0, 0, 0}, {0, v, 0, 0}, {0, 0, v, 0}, {0, 0, 0, v} } {}

    inline explicit m4_t(m3_t<T> v):
            m { {v.m00, v.m01, v.m02, 0}, {v.m10, v.m11, v.m12, 0}, {v.m20, v.m21, v.m22, 0}, {0, 0, 0, 1} } {}

    /// The identity matrix.
    inline static m4_t<T> id() {
        return m4_t {
            v4{1, 0, 0, 0},
            v4{0, 1, 0, 0},
            v4{0, 0, 1, 0},
            v4{0, 0, 0, 1},
        };
    }

    /// Calculate the determinant
    inline T determinant() const {
        return
            m00 * ( m11 * (m22 * m33 - m23 * m32) +
                    m12 * (m23 * m31 - m21 * m33) +
                    m13 * (m21 * m32 - m22 * m31)) +
            m01 * ( m10 * (m23 * m32 - m22 * m33) +
                    m12 * (m20 * m33 - m23 * m30) +
                    m13 * (m22 * m30 - m20 * m32)) +
            m02 * ( m10 * (m21 * m33 - m23 * m31) +
                    m11 * (m23 * m30 - m20 * m33) +
                    m13 * (m20 * m31 - m21 * m30)) +
            m03 * ( m10 * (m22 * m31 - m21 * m32) +
                    m11 * (m20 * m32 - m22 * m30) +
                    m12 * (m21 * m30 - m20 * m31));
    }

    /// Transpose the matrix
    inline m4_t<T> transpose() const {
        return m4_t{
            v4_t { m00, m10, m20, m30 },
            v4_t { m01, m11, m21, m31 },
            v4_t { m02, m12, m22, m32 },
            v4_t { m03, m13, m23, m33 },
        };
    }

    /// Get the matrix's inverse
    inline m4_t<T> invert() const {
        auto a2323 = m22 * m33 - m23 * m32;
        auto a1323 = m21 * m33 - m23 * m31;
        auto a1223 = m21 * m32 - m22 * m31;
        auto a0323 = m20 * m33 - m23 * m30;
        auto a0223 = m20 * m32 - m22 * m30;
        auto a0123 = m20 * m31 - m21 * m30;
        auto a2313 = m12 * m33 - m13 * m32;
        auto a1313 = m11 * m33 - m13 * m31;
        auto a1213 = m11 * m32 - m12 * m31;
        auto a2312 = m12 * m23 - m13 * m22;
        auto a1312 = m11 * m23 - m13 * m21;
        auto a1212 = m11 * m22 - m12 * m21;
        auto a0313 = m10 * m33 - m13 * m30;
        auto a0213 = m10 * m32 - m12 * m30;
        auto a0312 = m10 * m23 - m13 * m20;
        auto a0212 = m10 * m22 - m12 * m20;
        auto a0113 = m10 * m31 - m11 * m30;
        auto a0112 = m10 * m21 - m11 * m20;

        auto det = m00 * ( m11 * a2323 - m12 * a1323 + m13 * a1223 )
                  - m01 * ( m10 * a2323 - m12 * a0323 + m13 * a0223 )
                  + m02 * ( m10 * a1323 - m11 * a0323 + m13 * a0123 )
                  - m03 * ( m10 * a1223 - m11 * a0223 + m12 * a0123 ) ;

        if (det == 0.0) throw std::logic_error("m4_t::invert on non-invertible matrix");
        det = (T)1 / det;

        return m4_t {
            v4_t {
                det * (m11 * a2323 - m12 * a1323 + m13 * a1223),
                det * -(m01 * a2323 - m02 * a1323 + m03 * a1223),
                det * (m01 * a2313 - m02 * a1313 + m03 * a1213),
                det * -(m01 * a2312 - m02 * a1312 + m03 * a1212),
            },
            v4_t{
                det * -(m10 * a2323 - m12 * a0323 + m13 * a0223),
                det * (m00 * a2323 - m02 * a0323 + m03 * a0223),
                det * -(m00 * a2313 - m02 * a0313 + m03 * a0213),
                det * (m00 * a2312 - m02 * a0312 + m03 * a0212),
            },
            v4_t{
                det * (m10 * a1323 - m11 * a0323 + m13 * a0123),
                det * -(m00 * a1323 - m01 * a0323 + m03 * a0123),
                det * (m00 * a1313 - m01 * a0313 + m03 * a0113),
                det * -(m00 * a1312 - m01 * a0312 + m03 * a0112),
            },
            v4_t{
                det * -(m10 * a1223 - m11 * a0223 + m12 * a0123),
                det * (m00 * a1223 - m01 * a0223 + m02 * a0123),
                det * -(m00 * a1213 - m01 * a0213 + m02 * a0113),
                det * (m00 * a1212 - m01 * a0212 + m02 * a0112),
            }
        };
    }

    inline m4_t<T> operator*(const m4_t<T>& a) const {
        auto b = *this;
        auto out = m4_t{};
        for (auto r = 0u; r < 4; r++)
            for (auto c = 0u; c < 4; c++)
                out.m[r][c] = a.m[r][0] * b.m[0][c] + a.m[r][1] * b.m[1][c] + a.m[r][2] * b.m[2][c] + a.m[r][3] * b.m[3][c];

        return out;
    }

    inline m4_t<T> operator*(T v) const {
        auto b = *this;
        auto out = m4_t{};
        for (auto r = 0u; r < 4; r++) for (auto c = 0u; c < 4; c++) out.m[r][c] = m[r][c] * v;
        return out;
    }

    // M * (v.x, v.y, v.x, 1)
    inline v3_t<T> operator*(v3_t<T> v) const {
        return v3_t<T> {
            m00 * v.x + m10 * v.y + m20 * v.z + m30,
            m01 * v.x + m11 * v.y + m21 * v.z + m31,
            m02 * v.x + m12 * v.y + m22 * v.z + m32,
        };
    }

    inline v4_t<T> operator*(v4_t<T> v) const {
        return v4_t<T> {
            m00 * v.x + m10 * v.y + m20 * v.z + m30 * v.w,
            m01 * v.x + m11 * v.y + m21 * v.z + m31 * v.w,
            m02 * v.x + m12 * v.y + m22 * v.z + m32 * v.w,
            m03 * v.x + m13 * v.y + m23 * v.z + m33 * v.w,
        };
    }

    bool operator==(const m4_t &rhs) const {
        return relative_eq(m00, rhs.m00) && relative_eq(m01, rhs.m01) && relative_eq(m02, rhs.m02) && relative_eq(m03, rhs.m03) &&
               relative_eq(m10, rhs.m10) && relative_eq(m11, rhs.m11) && relative_eq(m12, rhs.m12) && relative_eq(m13, rhs.m13) &&
               relative_eq(m20, rhs.m20) && relative_eq(m21, rhs.m21) && relative_eq(m22, rhs.m22) && relative_eq(m23, rhs.m23) &&
               relative_eq(m30, rhs.m30) && relative_eq(m31, rhs.m31) && relative_eq(m32, rhs.m32) && relative_eq(m33, rhs.m33);
    }

    bool operator!=(const m4_t &rhs) const {
        return !(rhs == *this);
    }
};

using m4 = m4_t<float>;

template <class T> std::ostream& operator<<(std::ostream& os, const m4_t<T>& m)  {
    os << "|" << m.m00 << ", " << m.m01 << ", " << m.m02 << ", " << m.m03 << "|" << std::endl;
    os << "|" << m.m10 << ", " << m.m11 << ", " << m.m12 << ", " << m.m13 << "|" << std::endl;
    os << "|" << m.m20 << ", " << m.m21 << ", " << m.m22 << ", " << m.m23 << "|" << std::endl;
    os << "|" << m.m30 << ", " << m.m31 << ", " << m.m32 << ", " << m.m33 << "|" << std::endl;
    return os;
}

// ================================================================= //
//                            Rotor
// ================================================================= //

/// A 3D rotation
struct r3 {
    float w = 1.0;
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;

    static r3 make(v3 from, v3 to) {
        auto v = from.cross(to);
        return r3{ 1.0f + to.dot(from), v.x, v.x, v.z }.normalized();
    }

    /// Construct a rotor from a normal and an angle
    static r3 from_angle(float angle, v3 normal) {
        auto n = normal.normalize() * std::sin(angle / 2.f);
        return r3 {
            std::cos(angle / 2.f), n.x, n.y, n.z
        };
    }

    /// Construct a rotor from euler angles
    static r3 from_euler(v3 angle) { // roll, yaw, pitch
        auto cr = std::cos(angle.x * 0.5f);
        auto sr = std::sin(angle.x * 0.5f);
        auto cy = std::cos(angle.y * 0.5f);
        auto sy = std::sin(angle.y * 0.5f);
        auto cp = std::cos(angle.z * 0.5f);
        auto sp = std::sin(angle.z * 0.5f);

        return r3 {
            cy * cp * cr - sy * sp * sr,
            sy * sp * cr + cy * cp * sr,
            sy * cp * cr + cy * sp * sr,
            cy * sp * cr - sy * cp * sr,
        };
    }

    inline v3 to_euler() {
        auto sinr_cosp = +2.0f * (w * x + y * z);
        auto cosr_cosp = +1.0f - 2.0f * (x * x + y * y);
        auto roll = std::atan2(sinr_cosp, cosr_cosp);

        auto sinp = +2.0f * (w * y - z * x);
        auto yaw = 0.0f;
        if (fabs(sinp) >= 1)
            yaw = std::copysign((float) M_PI / 2.0f, sinp); // use 90 degrees if out of range
        else
            yaw = std::asin(sinp);
        auto siny_cosp = +2.0f * (w * z + x * y);
        auto cosy_cosp = +1.0f - 2.0f * (y * y + z * z);
        auto pitch = std::atan2(siny_cosp, cosy_cosp);

        return v3 { roll, yaw, pitch };
    }

    bool is_valid() const {
        bool valid = std::isfinite(w) && std::isfinite(x) && std::isfinite(y) && std::isfinite(z) && relative_eq(length(), 1.0f);
        if (!valid) {
            abort();
        }
        return valid;
    }

    /// The squared length of the rotor
    inline float length_sq() const {
        return w*w + x*x + y*y + z*z;
    }

    /// The length of the rotor
    inline float length() const {
        return std::sqrt(length_sq());
    }

    inline r3 normalized() const {
        auto len = length();
        return r3 { w / len, x / len, y / len, z / len };
    }

    inline r3 invert() const {
        return r3 { w, -x, -y, -z };
    }

    inline r3 operator*(r3 const& b) const {
        return r3 {
            (w * b.w) - (x * b.x) - (y * b.y) - (z * b.z),
            (x * b.w) + (w * b.x) + (y * b.z) - (z * b.y),
            (y * b.w) + (w * b.y) + (z * b.x) - (x * b.z),
            (z * b.w) + (w * b.z) + (x * b.y) - (y * b.x)
        };
    }

    /// Rotate a vector by the rotor
    inline v3 rotate(v3 const& b) const {
        auto q = v3{ x, y, z };
        auto uv = -q.cross(b);
        auto uuv = -q.cross(uv);
        uv = uv * (2.0f * w);
        uuv = uuv * 2;
        return b + (uv + uuv);
    }

    /// Create a 3x3 rotation matrix from the rotor
    inline m3 to_matrix() const {
        return m3 (
            rotate(v3(1, 0, 0)),
            rotate(v3(0, 1, 0)),
            rotate(v3(0, 0, 1))
        ).transpose();
    }

    inline r3 operator*(float s) const { return r3 { w * s, x * s, y * s, z * s }; }
    inline r3 operator/(float s) const { return r3 { w / s, x / s, y / s, z / s }; }

    inline r3 operator+(r3 b) const { return r3 { w + b.w, x + b.x, y + b.y, z + b.z }; }

    inline r3 operator-() const { return r3 { -w, -x, -y, -z }; }

    bool operator==(const r3 &rhs) const {
        return relative_eq(w, rhs.w) &&
            relative_eq(x, rhs.x) &&
            relative_eq(y, rhs.y) &&
            relative_eq(z, rhs.z);
    }

    bool operator!=(const r3 &rhs) const {
        return !(rhs == *this);
    }
};

inline std::ostream& operator<<(std::ostream& os, const r3& r)  {
    os << "( " << r.w << ", " << r.x << ", " << r.y << ", " << r.z << " )";
    return os;
}

/// Interpolate between two rotors by the given factor
inline inline r3 slerp(r3 a, r3 b, float f) {
    assert(a.is_valid());
    assert(b.is_valid());
    float c = a.w*b.w + a.x*b.x + a.y*b.y + a.z*b.z;

    if (c < 0.0) {
        b = -b;
        c = -c;
    }

    auto res = r3{};
    if(c > 0.9995) {
        res = r3{ mix(a.w, b.w, f), mix(a.x, b.x, f), mix(a.y, b.y, f), mix(a.z, b.z, f) };
    } else {
        float angle = std::acos(c);
        res = (a * std::sin((1 - f) * angle) + b * std::sin(f * angle)) / std::sin(angle);
    }

    assert(res.is_valid());
    return res;
}

// ================================================================= //
//                        Ranges & Bounds
// ================================================================= //

/// A range of values
template<class T> struct range_t {
    T min, max;

    inline range_t(T a = 0, T b = 0, bool sort = true) {
        if (!sort || a < b) { max = b; min = a; }
        else { max = a; min = b; }
    }

    inline range_t(T a, T b, T c) {
        *this = range_t(a, b).combine(range_t(a, c));
    }

    /// Test if a value is contained in the range
    inline bool contains(const T& v) const {
        return v >= min && v <= max;
    }

    /// Test if this range contains another range
    inline bool contains(const range_t<T>& r) const {
        return min <= r.min && max >= r.max;
    }

    /// Test if this range intersects another
    inline bool intersects(const range_t<T>& other) const {
        return !(min > other.max || max < other.min);
    }

    /// Get the union of two ranges
    inline range_t<T> combine(const range_t<T>& other) const {
        return range_t(std::min(min, other.min), std::max(max, other.max));
    }

    /// Expand the range by the given distance in both directions
    inline range_t<T> expand(T distance) const {
        return range_t(min - distance, max + distance);
    }

    /// Get the width of the range
    inline T width() const {
        return (max - min);
    }

    /// Get the point in the center of the range
    inline T center() const {
        return min + (width() / 2);
    }

    inline T clamp(T v) {
        return ::clamp(v, min, max);
    }

    // Relational operators
    inline bool operator==(const range_t<T>& other) const {
        return min == other.min && max == other.max;
    }

    inline bool operator<(const range_t &rhs) const { return min < rhs.min; }
    inline bool operator>(const range_t &rhs) const { return rhs < *this; }
    inline bool operator<=(const range_t &rhs) const { return !(rhs < *this); }
    inline bool operator>=(const range_t &rhs) const { return !(*this < rhs); }

    inline range_t<T> operator+(T distance) const {
        return range_t(min + distance, max + distance);
    }

    inline range_t<T> operator*(T scale) const {
        return range_t(min * scale, max * scale);
    }

    inline range_t<T> operator-(T distance) const {
        return range_t(min - distance, max - distance);
    }

    friend std::ostream &operator<<(std::ostream &os, const range_t &range1) {
        os << "[" << range1.min << " to " << range1.max << "]";
        return os;
    }

    /// An iterator over the values in the range, mostly useful for integer ranges,
    /// the minimum and maximum values are included.
    struct it_state {
        T value;

        void next(const range_t<T>* ref) { value += 1; }
        void begin(const range_t<T>* ref) { value = ref->min; }
        void end(const range_t<T>* ref) { value = ref->max; }
        bool cmp(const it_state& s) const { return value <= s.value; }
        const T get(const range_t<T>* ref) const { return value; }
    };
    SETUP_CONST_ITERATOR(range_t<T>, T, it_state);
};

using range = range_t<float>;

/// Map v from one range to another with linear interpolation
template<class T> inline T map(const T& v, const range_t<T>& from, const range_t<T>& to) {
    auto value = clamp(v, from.min, from.max);
    auto k = (value - from.min) / (from.max - from.min);
    return to.min + ((to.max - to.min) * k);
}

/// A 2D axis-aligned bounding box
template<class T> struct bounds2_t {
    range_t<T> x, y;

    inline bounds2_t(range_t<T> x = range_t<T>(), range_t<T> y = range_t<T>()): x(x), y(y) {}

    /// Test if a point is contained in the bounds
    inline bool contains(const v2_t<T>& v) const {
        return x.contains(v.x) && y.contains(v.y);
    }

    /// Test if this AABB contains another
    inline bool contains(const bounds2_t<T>& other) const {
        return x.contains(other.x) && y.contains(other.y);
    }

    /// Test if this AABB intersects another
    inline bool intersects(const bounds2_t<T>& other) const {
        return !(x.min > other.x.max ||
                 x.max < other.x.min ||
                 y.min > other.y.max ||
                 y.max < other.y.min);
    }

    /// Get the union of two bounds
    inline bounds2_t<T> combine(const bounds2_t<T>& other) const {
        return bounds2_t(x.combine(other.x), y.combine(other.y));
    }

    /// Expand the bounds by the given distance in each direction
    inline bounds2_t<T> expand(T distance) const {
        return bounds2_t(x.expand(distance), y.expand(distance));
    }

    /// Get the perimeter of the bounds
    inline float perimeter() const {
        return 2.f * (x.width() + y.width());
    }

    /// Get the point in the center of the bounds
    inline v2_t<T> center() const {
        return vec2(x.center(), y.center());
    }

    inline v2_t<T> min() const { return vec2(x.min, y.min); }
    inline v2_t<T> max() const { return vec2(x.max, y.max); }
    inline v2_t<T> size() const { return vec2(x.width(), y.width()); }

    /// Get a point's position relative to the bounds
    inline v2_t<T> to_relative(v2_t<T> point) const {
        return v2_t<T>(point.x - x.min, point.y - y.min);
    }

    /// Get the absolute position of a relative position
    inline v2_t<T> to_absolute(v2_t<T> point) const {
        return v2_t<T>(point.x + x.min, point.y + y.min);
    }

    inline v2_t<T> lerp(v2_t<T> point) const {
        return point * size() + min();
    }

    inline bounds2_t<T> operator+(v2_t<T> v) const { return bounds2_t(x + v.x, y + v.y); }
    inline bounds2_t<T> operator-(v2_t<T> v) const { return bounds2_t(x - v.x, y - v.y); }

    inline bounds2_t<T> operator*(T scale) const { return bounds2_t(x * scale, y * scale); }
    inline bounds2_t<T> operator/(T scale) const { return bounds2_t(x / scale, y / scale); }

    inline bool operator==(const bounds2_t<T>& other) const {
        return x == other.x && y == other.y;
    }

    friend std::ostream &operator<<(std::ostream &os, const bounds2_t &bounds) {
        os << "x: " << bounds.x << " y: " << bounds.y;
        return os;
    }

    v2_t<T> clamp(v2_t<T> v) {
        return vec2(x.clamp(v.x), y.clamp(v.y));
    }
};

using bounds2 = bounds2_t<float>;


/// A 3D axis-aligned bounding box
template<class T> struct bounds3_t {
    range_t<T> x, y, z;

    explicit inline bounds3_t(
        range_t<T> x = range_t<T>(), range_t<T> y = range_t<T>(), range_t<T> z = range_t<T>()
    ): x(x), y(y), z(z) {}

    /// Get the bounds of a triangle.
    inline static bounds3_t<T> from_triangle(v3_t<T> a, v3_t<T> b, v3_t<T> c) {
        return bounds3_t { { a.x, b.x, c.x }, { a.y, b.y, c.y }, { a.z, b.z, c.z } };
    }

    /// Test if a point is contained in the bounds
    inline bool contains(const v3_t<T>& v) const {
        return x.contains(v.x) && y.contains(v.y) && z.contains(v.z);
    }

    /// Test if this AABB contains another
    inline bool contains(const bounds3_t<T>& other) const {
        return x.contains(other.x) && y.contains(other.y) && z.contains(other.z);
    }

    /// Test if this AABB intersects another
    inline bool intersects(const bounds3_t<T>& other) const {
        return x.intersects(other.x) && y.intersects(other.y) && z.intersects(other.z);
    }

    /// Get the union of two bounds
    inline bounds3_t<T> combine(const bounds3_t<T>& other) const {
        return bounds3_t(x.combine(other.x), y.combine(other.y), z.combine(other.z));
    }

    /// Expand the bounds by the given distance in each direction
    inline bounds3_t<T> expand(T distance) const {
        return bounds3_t(x.expand(distance), y.expand(distance), z.expand(distance));
    }

    /// Get the surface area of the bounds
    inline float surface_area() const {
        return 2.f * ( x.width()*y.width() + x.width()*z.width() + z.width()*y.width());
    }

    /// Get the point in the center of the bounds
    inline v3_t<T> center() const {
        return v3(x.center(), y.center(), z.center());
    }

    inline v3_t<T> min() const { return v3(x.min, y.min, z.min); }
    inline v3_t<T> max() const { return v3(x.max, y.max, z.max); }
    inline v3_t<T> size() const { return v3(x.width(), y.width(), z.width()); }

    inline bounds3_t<T> operator+(v3_t<T> v) const { return bounds3_t(x + v.x, y + v.y, z + v.z); }
    inline bounds3_t<T> operator-(v3_t<T> v) const { return bounds3_t(x - v.x, y - v.y, z - v.z); }

    inline bounds3_t<T> operator*(T scale) const { return bounds3_t(x * scale, y * scale, z * scale); }
    inline bounds3_t<T> operator/(T scale) const { return bounds3_t(x / scale, y / scale, z / scale); }

    inline bool operator==(const bounds2_t<T>& other) const {
        return x == other.x && y == other.y && z == other.z;
    }

    friend std::ostream &operator<<(std::ostream &os, const bounds3_t &bounds) {
        os << "x: " << bounds.x << " y: " << bounds.y << " z: " << bounds.z;
        return os;
    }

    v3_t<T> clamp(v3_t<T> v) {
        return v3(x.clamp(v.x), y.clamp(v.y), z.clamp(v.z));
    }
};

using bounds3 = bounds3_t<float>;

// ================================================================= //
//                         Transformations
// ================================================================= //

/// Create a 2D translation matrix.
inline m3 translate(v2 t) {
    return m3{
        v3{ 1.0, 0.0, 0.0 },
        v3{ 0.0, 1.0, 0.0 },
        v3{ t.x, t.y, 1.0 },
    };
}

inline m4 translate(v3 t) {
    return m4 {
        v4{1.0, 0.0, 0.0, 0.0},
        v4{0.0, 1.0, 0.0, 0.0},
        v4{0.0, 0.0, 1.0, 0.0},
        v4{t.x, t.y, t.z, 1.0}
    };
}

/// Create a transformation matrix which scales.
inline m3 scale(v2 s) {
    return m3 {
        v3{s.x, 0.0, 0.0},
        v3{0.0, s.y, 0.0},
        v3{0.0, 0.0, 1.0},
    };
}

inline m4 scale(v3 s) {
    return m4 {
        v4{s.x, 0.0, 0.0, 0.0},
        v4{0.0, s.y, 0.0, 0.0},
        v4{0.0, 0.0, s.z, 0.0},
        v4{0.0, 0.0, 0.0, 1.0}
    };
}

/// Construct a perspective projection matrix.
inline m4 perspective(float fovy, float aspect, float near, float far) {
    /*
		T const tanHalfFovy = tan(fovy / static_cast<T>(2));

		mat<4, 4, T, defaultp> Result(static_cast<T>(0));
		Result[0][0] = static_cast<T>(1) / (aspect * tanHalfFovy);
     */

    auto h = std::tan(fovy / 2.f);

    auto m = m4 { 0 };
    m.m00 = 1.f / (aspect * h);
    m.m11 = 1.f / h;
    m.m22 = -(far + near) / (far - near);
    m.m23 = -1.f;
    m.m32 = -(2.f * far * near) / (far - near);
    return m;
}

/// Construct an orthographic projection matrix.
inline m4 ortho(float left, float right, float bottom, float top, float near, float far) {
    return m4 {
        v4 { 2.f / (right - left), 0, 0, 0 },
        v4 { 0, 2.f / (top - bottom), 0, 0 },
        v4 { 0, 0, - 2.f / (far - near), 0 },
        v4 { - (right + left) / (right - left), - (top + bottom) / (top - bottom), - (far + near) / (far - near), 1 },
    };
}

/// Construct a view matrix pointing to some target from some position.
inline m4 look_at(v3 eye, v3 target, v3 up) {
    auto f = (target - eye).normalize();
    auto s = f.cross(up).normalize();
    auto u = s.cross(f);

    auto m = m4(1.f);

    m.m00 = s.x;
    m.m10 = s.y;
    m.m20 = s.z;
    m.m01 = u.x;
    m.m11 = u.y;
    m.m21 = u.z;
    m.m02 = -f.x;
    m.m12 = -f.y;
    m.m22 = -f.z;
    m.m30 = -s.dot(eye);
    m.m31 = -u.dot(eye);
    m.m32 = f.dot(eye);
    return m;
}

/// Create a transformation matrix which rotates by the given angle in degrees.
inline m3 rotate(float angle) {
    float s = std::sin(deg2rad(angle));
    float c = std::cos(deg2rad(angle));
    return m3 { v3{c, s, 0}, v3{-s, c, 0}, v3{0, 0, 1} };
}

/// Create a transformation matrix which rotates about a given point
inline m3 rotate_about(float angle, v2 pos) {
    return translate(pos) * rotate(angle) * translate(-pos);
}

/// Create a view matrix
inline m3 view(bounds2 bounds) {
    return scale(v2(1.f, -1.f))
           * translate(v2(-1.f, -1.f))
           * scale(v2(2.f / bounds.x.width(), 2.f / bounds.y.width()))
           * translate(v2(-bounds.x.min, -bounds.y.min));
}

inline m4 make_transform(v3 translation, v3 scale, r3 rotation) {
    //assert(rotation.is_valid());
    auto r = m4(rotation.to_matrix());
    auto s = ::scale(scale);
    auto t = ::translate(translation);
    return t * r * s;
}

/// A position, orientation, and scale in world-space.
struct transform2 {
    v2 position = v2(0.0f, 0.0f);
    float orientation = 0.0f;
    float scale = 1.0f;

    /// Get the interpolated transformation at some fractional time in ticks with
    /// the given velocity & angular velocity.
    inline transform2 interpolate(float t, v2 velocity, float angular_velocity) const {
        return transform2 { position + velocity * t, orientation + angular_velocity * t};
    }

    /// Combine two transformations. Scale is multiplied.
    inline transform2 operator+(transform2 const& rhs) const {
        // TODO: adjust position by scale & orientation
        return transform2 { position + rhs.position, orientation + rhs.orientation, scale * rhs.scale };
    }

    /// Get the transformation matrix which applies this transform to a point.
    inline m3 matrix() const {
        return ::translate(position) * ::rotate(orientation) * ::scale(v2(scale));
    }
};

#endif
