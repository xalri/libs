#ifndef XAL_LIBS_TUPLE_UTIL_HPP
#define XAL_LIBS_TUPLE_UTIL_HPP

#include <tuple>

// https://stackoverflow.com/a/16387374
namespace detail {
    template<int... Is>
    struct seq { };

    template<int N, int... Is>
    struct gen_seq : gen_seq<N - 1, N - 1, Is...> { };

    template<int... Is>
    struct gen_seq<0, Is...> : seq<Is...> { };

    template<typename T, typename F, int... Is>
    void for_each(T&& t, F f, seq<Is...>) {
        auto l = { (f(std::get<Is>(t)), 0)... };
    }
}

template<typename... Ts, typename F>
void for_each_in_tuple(std::tuple<Ts...> const& t, F f) {
    detail::for_each(t, f, detail::gen_seq<sizeof...(Ts)>());
}

// https://stackoverflow.com/a/21063041
template<int N, class T, class F>
void apply_one(T const& p, F func) {
    typename std::tuple_element<N, T>::type const& value = std::get<N>(p);
    func( value );
}

template<class T, class F, int... Is>
void apply(T const& p, int index, F func, detail::seq<Is...>) {
    //using FT = void(T const&, F);
    using FT = decltype(apply_one<0, T, F>);
    static constexpr FT* arr[] = { (&apply_one<Is, T, F>)... };
    arr[index](p, func);
}

template<class T, class F> void apply(T const& p, int index, F func) {
    apply(p, index, func, detail::gen_seq<std::tuple_size<T>::value>{});
}

#endif
