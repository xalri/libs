if (NOT TARGET xal_libs)
    project(xal_libs)

    add_library(xal_libs INTERFACE)
    target_sources(xal_libs INTERFACE
            ${PROJECT_SOURCE_DIR}/xal_buffer.h
            ${PROJECT_SOURCE_DIR}/xal_math.h
            ${PROJECT_SOURCE_DIR}/xal_collide.h
            ${PROJECT_SOURCE_DIR}/xal_prof.h
            ${PROJECT_SOURCE_DIR}/xal_time.h
            ${PROJECT_SOURCE_DIR}/xal_util.h
            ${PROJECT_SOURCE_DIR}/xal_smallvec.h
            ${PROJECT_SOURCE_DIR}/xal_storage.h
            ${PROJECT_SOURCE_DIR}/xal_scene_graph.h
            ${PROJECT_SOURCE_DIR}/xal_slab.h
            ${PROJECT_SOURCE_DIR}/xal_nav.h
            ${PROJECT_SOURCE_DIR}/xal_bvt.h
            ${PROJECT_SOURCE_DIR}/third_party/iterator_tpl.h
            ${PROJECT_SOURCE_DIR}/third_party/channel.hpp
            ${PROJECT_SOURCE_DIR}/third_party/member_check.h
            ${PROJECT_SOURCE_DIR}/third_party/tuple_util.hpp
            ${PROJECT_SOURCE_DIR}/third_party/catch.hpp
            )
    target_include_directories(xal_libs INTERFACE
            ${PROJECT_SOURCE_DIR}/
            ${PROJECT_SOURCE_DIR}/third_party/
            )

    add_executable(xal_libs_tests
            tests/main.cpp
            tests/collide.cpp
            tests/math.cpp
            )
    target_link_libraries(xal_libs_tests xal_libs)
endif()
