#ifndef XAL_LIBS_SCENE_GRAPH_HPP
#define XAL_LIBS_SCENE_GRAPH_HPP

#include <cassert>
#include <vector>
#include "xal_math.h"

/// A node in the scene graph
struct Node {
    v3 translation = v3(0.0f);
    r3 rotation = r3();
    v3 scale = v3(1.0f);
    int parent = -1; // The index of the node's parent
    size_t level = 1; // The level of the tree at this node

    /// Get the local transformation matrix for this node
    inline m4 local_transform() const {
        return make_transform(translation, scale, rotation);
    }
};

/// A minimal scene graph.
struct SceneGraph {
    std::vector<Node> nodes = std::vector<Node>{};
    std::vector<m4> global_transforms = std::vector<m4>{}; // Local space -> world space transforms.
    size_t tree_depth = 0;

    /// Update world space transforms.
    inline void update() {
        global_transforms.resize(nodes.size(), m4(1.0f));

        // Update top level nodes
        for(auto i = 0u; i < nodes.size(); i++) {
            if (nodes[i].level == 1) {
                global_transforms[i] = nodes[i].local_transform();
            }
        }

        // Iteratively update children
        for (auto level = 2u; level <= tree_depth; level++) {
            for (auto i = 0u; i < nodes.size(); i++) {
                if (nodes[i].level == level)
                    global_transforms[i] = global_transforms[nodes[i].parent] * nodes[i].local_transform();
            }
        }
    }
    
    inline void fix_levels() {
        for (auto i = 0u; i < nodes.size(); i++) {
            auto j = i;
            while(nodes[j].parent >= 0) {
                nodes[i].level += 1;
                j = (unsigned int) nodes[j].parent;
                if (nodes[i].level > tree_depth) tree_depth = nodes[i].level;
            }
        }
    }
};

#endif
