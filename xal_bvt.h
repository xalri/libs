// A bounding volume tree broadphase using the surface area heuristic.
// Based on Box2D's dynamic AABB tree.
//
// Copyright (c) 2009 Erin Catto http://www.box2d.org
// Modified by Lewis Hallam <lewis@xa1.uk>, 2018
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.


#include <vector>
#include <cstdint>

#include "xal_math.h"
#include "xal_smallvec.h"

struct Bvt {
    struct Node {
        int32_t parent;
        int32_t left; // also user-data
        int32_t right;
        bounds3 bounds;
        int32_t next_free; // used by non-free nodes for temporary data in various algorithms.
        int32_t height;
        inline static int32_t null = -1;

        bool is_leaf() const {
            return right == null;
        }
    };

    std::vector<Node> nodes {};
    int32_t root = Node::null;
    int32_t free_list = 0;
    mutable smallvec<int32_t, 256> stack;
    mutable smallvec<std::pair<int32_t, int32_t>, 256> pair_stack;

    /// Create a BVT.
    Bvt() { expand(16); }

    /// Add a node to the tree, returns the node's ID.
    int32_t add_node(bounds3 bounds, int32_t user_data = 0) {
        auto id = alloc_node();
        nodes[id].bounds = bounds;
        nodes[id].height = 0;
        nodes[id].left = user_data;
        insert_leaf(id);
        return id;
    }

    /// Clear the BVT
    void clear() {
        nodes.clear();
        root = Node::null;
        expand(16);
    }

    /// Query for overlapping bounds. The callback is given the user data for every
    /// overlapping node, the query ends when the callback returns false or there are
    /// no more overlapping nodes.
    template<class F>
    void query(bounds3 bounds, F callback) const {
        stack.push(root);
        while (stack.size() > 0) {
            auto id = stack.pop();
            if (id == Node::null) continue;

            auto& node = nodes[id];

            if (node.bounds.intersects(bounds)) {
                if (node.is_leaf()) {
                    if (!callback(node.left)) {
                        stack.stack_size = 0;
                        return;
                    }
                } else {
                    stack.push(node.left);
                    stack.push(node.right);
                }
            }
        }
    }


    /// Find all overlapping pairs of leaf nodes
    template<class F>
    void overlapping_pairs(F callback) {
        if (root == Node::null) return;

        for (auto& n : nodes) if (!n.is_leaf()) n.next_free = 0;

        auto& root = nodes[this->root];

        if (root.is_leaf()) return;

        auto& stack = this->pair_stack;
        stack.clear();

        while (stack.size() > 0) {
            int32_t a, b;
            std::tie(a, b) = stack.pop();

            auto& node_a = this->nodes[a];
            auto& node_b = this->nodes[b];
        }
    }

    void insert_leaf(int32_t leaf) {
        if (root == Node::null) {
            root = leaf;
            nodes[root].parent = Node::null;
            return;
        }

        // Find the best sibling for this node with the surface area heuristic
        auto leaf_aabb = nodes[leaf].bounds;
        auto idx = root;
        while (!nodes[idx].is_leaf()) {
            auto l = nodes[idx].left;
            auto r = nodes[idx].right;

            auto area = nodes[idx].bounds.surface_area();

            auto combined_bounds = nodes[idx].bounds.combine(leaf_aabb);
            auto combined_area = combined_bounds.surface_area();

            // Cost of creating a new parent for this node and the new leaf
            auto cost = 2.0f * combined_area;

            //  Minimum cost of pushing the leaf further down the tree
            auto inheritance_cost = 2.0f * (combined_area - area);

            auto get_cost = [&](int32_t child) {
                auto aabb = leaf_aabb.combine(nodes[child].bounds);
                if (nodes[child].is_leaf()) {
                    return aabb.surface_area() + inheritance_cost;
                } else {
                    auto old_area = nodes[child].bounds.surface_area();
                    auto new_area = aabb.surface_area();
                    return (new_area - old_area) + inheritance_cost;
                }
            };

            auto cost_l = get_cost(l);
            auto cost_r = get_cost(r);

            if (cost < cost_l && cost < cost_r) break;

            if (cost_l < cost_r) idx = l;
            else idx = r;
        }

        auto sibling = idx;

        // Create a new parent.
        auto old_parent = nodes[sibling].parent;
        auto new_parent = alloc_node();
        nodes[new_parent].parent = old_parent;
        nodes[new_parent].bounds = leaf_aabb.combine(nodes[sibling].bounds);
        nodes[new_parent].height = nodes[sibling].height + 1;

        if (old_parent != Node::null) {
            auto& parent = nodes[old_parent];
            if (parent.left == sibling) {
                parent.left = new_parent;
            } else {
                parent.right = new_parent;
            }
        } else {
            root = new_parent;
        }

        nodes[new_parent].left = sibling;
        nodes[new_parent].right = leaf;
        nodes[sibling].parent = new_parent;
        nodes[leaf].parent = new_parent;

        // Walk back up the tree fixing heights and AABBs
        idx = nodes[leaf].parent;
        while (idx != Node::null) {
            idx = balance(idx);

            auto l = nodes[idx].left;
            auto r = nodes[idx].right;

            nodes[idx].height = 1 + std::max(nodes[l].height, nodes[r].height);
            nodes[idx].bounds = nodes[l].bounds.combine(nodes[r].bounds);

            idx = nodes[idx].parent;
        }
    }

    int32_t alloc_node() {
        if (free_list == Node::null) {
            expand(nodes.size() * 2);
        }

        auto id = free_list;
        free_list = nodes[id].next_free;
        nodes[id].parent = Node::null;
        nodes[id].left = Node::null;
        nodes[id].right = Node::null;
        nodes[id].height = 0;

        return id;
    }

    void expand(size_t capacity) {
        free_list = (int32_t) nodes.size();

        while (nodes.size() < capacity) {
            auto next = nodes.size() + 1;
            nodes.push_back(Node {
                0, {}, {}, bounds3{}, (int32_t) next, Node::null
            });
        }
        nodes.push_back(Node {
            0, {}, {}, bounds3{}, Node::null, 0
        });
    }

    int32_t balance(int32_t a_idx) {
          auto& a = nodes[a_idx];
          if (a.is_leaf() || a.height < 2) {
             return a_idx;
          }

          auto b_idx = a.left;
          auto c_idx = a.right;

          auto& b = nodes[b_idx];
          auto& c = nodes[c_idx];

          auto balance = c.height - b.height;

          // Rotate C up
          if (balance > 1) {
             auto f_idx = c.left;
             auto g_idx = c.right;
             auto& f = nodes[f_idx];
             auto& g = nodes[g_idx];

             // Swap a and C
             c.left = a_idx;
             c.parent = a.parent;
             a.parent = c_idx;

             // a's old parent should point to C
             if (c.parent != Node::null) {
                auto& parent = nodes[c.parent];
                if (parent.left == a_idx) {
                   parent.left = c_idx;
                } else {
                   parent.right = c_idx;
                }
             } else {
                root = c_idx;
             }

             // Rotate
             if (f.height > g.height) {
                c.right = f_idx;
                a.right = g_idx;
                g.parent = a_idx;
                a.bounds = b.bounds.combine(g.bounds);
                c.bounds = a.bounds.combine(f.bounds);

                a.height = 1 + std::max(b.height, g.height);
                c.height = 1 + std::max(a.height, f.height);
             } else {
                c.right = g_idx;
                a.right = f_idx;
                f.parent = a_idx;
                a.bounds = b.bounds.combine(f.bounds);
                c.bounds = a.bounds.combine(g.bounds);

                a.height = 1 + std::max(b.height, f.height);
                c.height = 1 + std::max(a.height, g.height);
             }

             return c_idx;
          }

          // Rotate b up
          if (balance < -1) {
             auto d_idx = b.left;
             auto e_idx = b.right;
             auto& d = nodes[d_idx];
             auto& e = nodes[e_idx];

             // Swap a and b
             b.left = a_idx;
             b.parent = a.parent;
             a.parent = b_idx;

             // a's old parent should point to b
             if (b.parent != Node::null) {
                auto& parent = nodes[b.parent];
                if (parent.left == a_idx) {
                   parent.left = b_idx;
                } else {
                   parent.right = b_idx;
                }
             } else {
                root = b_idx;
             }

             // Rotate
             if (d.height > e.height) {
                b.right = d_idx;
                a.left = e_idx;
                e.parent = a_idx;
                a.bounds = c.bounds.combine(e.bounds);
                b.bounds = a.bounds.combine(d.bounds);

                a.height = 1 + std::max(c.height, e.height);
                b.height = 1 + std::max(a.height, d.height);
             } else {
                b.right = e_idx;
                a.left = d_idx;
                d.parent = a_idx;
                a.bounds = c.bounds.combine(d.bounds);
                b.bounds = a.bounds.combine(e.bounds);

                a.height = 1 + std::max(c.height, d.height);
                b.height = 1 + std::max(a.height, e.height);
             }

             return b_idx;
          }

          return a_idx;
       }
};