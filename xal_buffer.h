#ifndef XAL_LIBS_BUFFER_HPP
#define XAL_LIBS_BUFFER_HPP

#include <cstdint>
#include <optional>
#include <vector>
#include <array>
#include <algorithm>
#include <atomic>
#include "iterator_tpl.h"

/// A fixed size circular buffer
template<class T, unsigned N> class Buffer {
	std::atomic_uint32_t idx { 0 };
    std::array<T, N> data{};

public:

    Buffer() = default;
    Buffer(Buffer&& rhs) noexcept: idx(rhs.idx.load()), data(rhs.data) {}
    Buffer& operator=(Buffer&& rhs) noexcept {
        auto tmp = idx.load();
        idx = rhs.idx.load();
        rhs.idx = tmp;
        std::swap(data, rhs.data);
        return *this;
    }

    /// Returns true if no items have been added to the buffer.
    bool empty() const { return data.empty(); }

    /// Returns the number of items in the buffer
    uint64_t size() const { return data.size(); }

    uint32_t get_idx() const { return idx.load(); }

    /// Add a value to the end of the buffer
    void add(T value) {
        auto idx = this->idx++;
        data[idx % N] = std::move(value);
    }

    /// Get a value from the buffer. Returns null if the value is out of range.
    T const* get(uint64_t index) const {
        auto idx = this->idx.load();
        auto overflow = (idx > N) ? idx - N : 0;
        if (index > overflow + data.size() - 1) return nullptr;
        if (index < overflow) return nullptr;
        return &data[index % N];
    }

    struct it_state {
        uint64_t pos;
        inline void next(const Buffer* _ref) { pos++; }
        inline void begin(const Buffer* _ref) { pos = 0ull; }
        inline void end(const Buffer* ref) { pos = std::min<uint64_t>(ref->idx.load(), ref->data.size()); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline T const& get(const Buffer* ref) const { return ref->data[pos]; }
    };
    SETUP_CONST_ITERATOR(Buffer, T, it_state);
};

#pragma once
#endif
