#ifndef XAL_LIBS_COLLIDE_HPP
#define XAL_LIBS_COLLIDE_HPP

#include <array>
#include <cassert>
#include <optional>
#include <ostream>
#include "xal_math.h"

/// A sphere in world-space
struct Sphere {
    float radius = 0.f;
    v3 position {};
    v3 linear_vel {};

    bounds3 bounds() const {
        return bounds3 {
            range { position.x, position.x + linear_vel.x }.expand(radius),
            range { position.y, position.y + linear_vel.y }.expand(radius),
            range { position.z, position.z + linear_vel.z }.expand(radius),
        };
    }
};

/// A triangle in world-space
struct Triangle {
    v3 a{}, b{}, c{};
};

struct Plane {
    v3 normal;
    float dist;

    /// Get the plane of a triangle
    inline static Plane from_triangle(Triangle t) {
        auto normal = (t.b - t.a).cross(t.a - t.c).normalize();
        auto dist = normal.dot(t.a);
        return Plane{ normal, dist };
    }

    inline float signed_dist(v3 point) const {
        return normal.dot(point) - dist;
    }
};

/*
/// A collection of collision volumes and assorted metadata for fast collision detection.
struct CollisionWorld {
    // ...
    size_t add_sphere(Sphere s, size_t mask, size_t group);
    size_t add_polyhedron(Polyhedron p);
    size_t add_triangle(Triangle t);
};
 */

/// An intersection between two bodies
struct Intersection {
    v3 normal {}; // Normalized collision normal
    std::optional<float> overlap = {}; // Minimum existing overlap
    std::optional<float> time = {}; // Time in ticks until collision

    bool operator==(const Intersection &rhs) const {
        return normal == rhs.normal &&
            (overlap.has_value() == rhs.overlap.has_value()) &&
            (!overlap.has_value() || (relative_eq(*overlap, *rhs.overlap))) &&
            (time.has_value() == rhs.time.has_value()) &&
            (!time.has_value() || (relative_eq(*time, *rhs.time)));
    }

    bool operator!=(const Intersection &rhs) const {
        return !(rhs == *this);
    }

    friend std::ostream &operator<<(std::ostream &os, const Intersection &i) {
        os << "normal: " << i.normal;
        if (i.overlap.has_value()) os <<" overlap: " << *i.overlap;
        if (i.time.has_value()) os << " time: " << *i.time;
        return os;
    }
};

/// A collision between two bodies.
struct Contact {
    Intersection intersection;
    v3 support_a; // local-space support points
    v3 support_b;

    /// Check if there is any collision
    inline bool has_value() const {
        return intersection.time.has_value();
    }

    /// Get the smaller of two contacts
    static Contact min(Contact const& a, Contact const& b) {
        if (!a.has_value()) return b;
        if (!b.has_value()) return a;
        return a.intersection.time < b.intersection.time ? a : b;
    }
};

/// The information about a body needed to resolve a collision
struct Body {
    v3 position{};
    v3 linear_vel{};
    r3 orientation{};
    v3 angular_vel{};

    m3 inv_inertia = m3{1.0};
    float inv_mass = 1.0;
    float friction = 0.2f;
    float elasticity = 0.75f;
    float separation = 0.4f;

    void integrate() {
        position = position + linear_vel;
        auto r = r3 { 0, angular_vel.x, angular_vel.y, angular_vel.z } * 0.5 * orientation;
        orientation = (orientation + r).normalized();

        auto v = linear_vel.length_sq();
        auto linear_damping_factor = 1.0f - 1.0f / ( (v * 100000.0) + (1.0 / 0.04f) );
        auto angular_damping_factor = 1.0f - 1.0f / ( (v * 100000.0) + (1.0 / 0.06f) );

        linear_vel = linear_vel * linear_damping_factor;
        angular_vel = angular_vel * angular_damping_factor;
    }
};

/// A change to a body which resolves a collision.
struct Resolution {
    v3 displacement {};
    v3 linear_impulse {};
    v3 angular_impulse {};

    void apply(Body& b) const {
        b.position = b.position + displacement;
        b.linear_vel = b.linear_vel + linear_impulse;
        b.angular_vel = b.angular_vel + angular_impulse;
    }
};

// Local space support point
inline v3 support_point(Sphere const& s, v3 normal) {
    return normal * s.radius;
}

// Volume of a sphere
inline float mass(Sphere const& s) {
    return static_cast<float>((4.0 / 3.0) * M_PI * s.radius * s.radius * s.radius);
}

// Inertial tensor of a solid sphere with constant density 1.
inline m3 inertia(Sphere const& s) {
    return m3((float)((2.0 / 5.0) * mass(s) * s.radius * s.radius));
}

inline v3 closest_point(Triangle const& t, v3 p) {
    // From Real Time Collision Detection by Christer Ericson (2004)
    auto ab = t.b - t.a;
    auto ac = t.c - t.a;
    auto ap = p - t.a;
    auto d1 = ab.dot(ap);
    auto d2 = ac.dot(ap);
    if (d1 <= 0.0f && d2 < 0.0f)  return t.a;

    auto pb = p - t.b;
    auto d3 = ab.dot(pb);
    auto d4 = ac.dot(pb);
    if (d3 >= 0.0f && d4 <= d3) return t.b;

    auto vc = d1 * d4 - d3 * d2;
    if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
        auto v = d1 / (d1 - d3);
        return t.a + ab * v;
    }

    auto cp = p - t.c;
    auto d5 = ab.dot(cp);
    auto d6 = ac.dot(cp);
    if (d6 >= 0.0f && d5 <= d6) return t.c;

    auto vb = d5 * d2 - d1 * d6;
    if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
        auto w = d2 / (d2 - d6);
        return t.a + ac * w;
    }

    auto va = d3 * d6 - d5 * d4;
    if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
        auto w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
        return t.b + (t.c - t.b) * w;
    }

    auto denom = 1.0f / (va + vb + vc);
    auto v = vb * denom;
    auto w = vc * denom;
    return t.a + ab * v + ac * w;
}

// Swept sphere-plane intersection
inline Intersection intersect(Sphere const& s, Plane const& p, v3* q = nullptr) {
    auto dist = p.signed_dist(s.position);

    // Check for existing overlap
    if (std::abs(dist) < s.radius) return Intersection { -p.normal, { dist }, { 0.0 } };

    auto d = p.normal.dot(s.linear_vel);
    if (d * dist >= 0.0) return Intersection{}; // Moving away from the plane

    auto r = dist >= 0.0 ? s.radius : -s.radius;
    auto time = (r - dist) / d;
    if (time > 1.0) return Intersection{};
    if (q != nullptr) *q = s.position + s.linear_vel * time - p.normal * r;
    return Intersection { -p.normal, {}, { time }};
}

inline Contact collide(Sphere const& s, Plane const& p) {
    Contact c{};
    auto q = v3{};
    c.intersection = intersect(s, p, &q);
    if (c.intersection.normal != v3{0}) {
        c.support_a = support_point(s, c.intersection.normal);
        c.support_b = q; // TODO
    }
    return c;
}

// Swept sphere-sphere intersection
inline Intersection intersect(Sphere const& s_a, Sphere const& s_b) {
    // Collide in the reference frame of body A
    auto p = s_b.position - s_a.position;
    auto v = s_b.linear_vel - s_a.linear_vel;
    auto radius = s_a.radius + s_b.radius;
    auto radius_sq = radius * radius;

    if (radius_sq > p.length_sq()) {
        auto overlap = radius - p.length();
        return Intersection { p.normalize(), { overlap }, { 0.0 } };
    }

    if (p.dot(v) > 0) return Intersection{}; // Spheres are separating

    auto d = v.normalize().dot(-p);
    auto f_sq = p.length_sq() - (d * d);
    auto g = radius_sq - f_sq;

    if (g < 0) return Intersection{};

    auto t = (d - std::sqrt(g)) / v.length();

    if (t > 1) return Intersection{};
    else return Intersection { (p + (v * t)).normalize(), {}, { t } };
}

// Get the intersection & support points for a sphere-sphere collision.
// If the spheres don't collide intersection time & overlap will be nullopt.
inline Contact collide(Sphere const& s_a, Sphere const& s_b) {
    Contact c{};
    c.intersection = intersect(s_a, s_b);
    if (c.intersection.normal != v3{0}) {
        c.support_a = support_point(s_a, c.intersection.normal);
        c.support_b = support_point(s_b, -c.intersection.normal);
    }
    return c;
}

/// Test if a point is inside a triangle
inline bool point_in_triangle(v3 p, Triangle t) {
    v3 e10 = t.b - t.a;
    v3 e20 = t.c - t.a;
    auto a = e10.dot(e10);
    auto b = e10.dot(e20);
    auto c = e20.dot(e20);
    auto ac_bb=(a*c)-(b*b);
    auto vp = p - t.a;
    auto d = vp.dot(e10);
    auto e = vp.dot(e20);
    auto x = (d*c)-(e*b);
    auto y = (e*a)-(d*b);
    auto z = x+y-ac_bb;
    return z < 0 && x >= 0 && y >= 0;
}

/// Find the smallest positive root of a quadratic equation below some threshold
inline std::optional<double> lowest_root(float a, float b, float c, float max) {
    auto det = b * b - 4.0 * a * c;
    if (det < 0.0) return {};

    auto sqrt_det = std::sqrt(det);
    auto r1 = (-b - sqrt_det) / (2.0 * a);
    auto r2 = (-b + sqrt_det) / (2.0 * a);

    auto x1 = std::min(r1, r2);
    auto x2 = std::max(r1, r2);

    if (x1 > 0.0 && x1 < max) return { x1 };
    if (x2 > 0.0 && x2 < max) return { x2 };
    return {};
}

/// Swept sphere-triangle collision.
inline Contact collide(Sphere sphere, Triangle t) {
    // Move into unit-sphere space.
    t = Triangle { t.a / sphere.radius, t.b / sphere.radius, t.c / sphere.radius };
    auto s = Sphere { 1.0f, sphere.position / sphere.radius, sphere.linear_vel / sphere.radius };

    auto plane = Plane::from_triangle(t);

    auto t0 = 0.0f, t1 = 0.0f;
    auto in_plane = false;

    auto dist = plane.signed_dist(s.position);
    auto nv = plane.normal.dot(s.linear_vel);
    if (nv == 0.0) {
        // Moving parallel to the plane
        if (std::abs(dist) >= 1.0) return {}; // Too distant from the plane to collide
        in_plane = true;
        t0 = 0.0f;
        t1 = 1.0f;
    } else {
        t0 = (-1.0f - dist) / nv;
        t1 = ( 1.0f - dist) / nv;

        if (t0 > t1) std::swap(t0, t1);
        if (t0 > 1.0f || t1 < -1.0f) return {};

        t0 = clamp(t0, 0.0f, 1.0f);
        t1 = clamp(t1, 0.0f, 1.0f);
    }

    auto time = t1; // time to collision
    auto point = std::optional<v3>{}; // sphere-space collision point on the triangle.

    if (!in_plane) {
        auto intersection = (s.position - plane.normal) + s.linear_vel * t0;
        if (point_in_triangle(intersection, t)) {
            time = t0;
            point = {intersection};
        }
    }

    if (!point.has_value()) {
        auto speed_sq = s.linear_vel.length_sq();

        auto test_corner = [&](v3 corner) {
            auto corner_time = lowest_root(
                speed_sq,
                2.0f * (s.linear_vel.dot(s.position - corner)),
                (corner - s.position).length_sq() - 1.0f,
                time);

            if (corner_time.has_value()) {
                point = {corner};
                time = (float) *corner_time;
            }
        };

        test_corner(t.a);
        test_corner(t.b);
        test_corner(t.c);

        auto test_edge = [&](v3 edge_a, v3 edge_b) {
            auto ab = edge_b - edge_a;
            auto pos = edge_a - s.position;
            auto edge_length_sq = ab.length_sq();
            auto edge_dot_v = ab.dot(s.linear_vel);
            auto edge_dot_pos = ab.dot(pos);
            auto edge_time = lowest_root(
                edge_length_sq * -speed_sq + edge_dot_v * edge_dot_v,
                edge_length_sq * (2.0f * s.linear_vel.dot(pos)) - 2.0f * edge_dot_v * edge_dot_pos,
                edge_length_sq * (1.0f - pos.length_sq()) + edge_dot_pos * edge_dot_pos,
                time);
            if (edge_time.has_value()) {
                auto f = (edge_dot_v * (*edge_time) - edge_dot_pos) / edge_length_sq;
                if (f >= 0.0f && f <= 1.0f) { // collision is inside edge
                    point = {edge_a + ab * f};
                    time = (float) *edge_time;
                }
            }
        };

        test_edge(t.a, t.b);
        test_edge(t.b, t.c);
        test_edge(t.c, t.a);
    }

    if (point.has_value()) {
        auto triangle_point = (*point) * sphere.radius; // convert back to world space
        auto sphere_pos = sphere.position + sphere.linear_vel * time;
        auto normal = v3{ triangle_point - sphere_pos }.normalize();
        auto sphere_point = support_point(sphere, normal);
        auto overlap = std::optional<float>{};

        auto diff = triangle_point - sphere_point;
        if (diff.dot(normal) > 0) {
            //overlap = { diff.length() };
            //overlap = { (1.0f - (s_pos - *point).length()) * sphere.radius };
            //std::cout << "An overlap " << *overlap << std::endl;
        }

        return Contact {
            Intersection { normal, overlap, { time } },
            sphere_point,
            triangle_point,
        };
    }

    return {};
}

// Impulse based collision response.
inline std::pair<Resolution, Resolution> resolve(Contact const& c, Body const& body_a, Body const& body_b) {
    auto res_a = Resolution{};
    auto res_b = Resolution{};

    if (body_a.inv_mass + body_b.inv_mass == 0.0) return { res_a, res_b };

    auto n = c.intersection.normal;
    auto tangent_vectors = compute_basis(n);

    auto elasticity = std::min(body_b.elasticity, body_a.elasticity);
    auto friction = std::sqrt(body_b.friction * body_a.friction);

    auto pa = body_a.position;    auto pb = body_b.position;
    auto ma = body_a.inv_mass;    auto mb = body_b.inv_mass;
    auto ia = body_a.inv_inertia; auto ib = body_b.inv_inertia;
    auto va = body_a.linear_vel;  auto vb = body_b.linear_vel;
    auto wa = body_a.angular_vel; auto wb = body_b.angular_vel;
    auto ra = c.support_a;        auto rb = c.support_b;

    // TODO: this is probably fucked
    if (c.intersection.overlap.has_value()) {
        auto ca = ra + pa;
        auto cb = rb + pb;
        auto d = cb - ca;
        auto separation = std::max(body_a.separation, body_b.separation);
        auto multiplier = separation / (ma + mb);

        if (ma > 0.f) {
            res_a.displacement = d * ma * multiplier;
            assert(res_a.displacement.is_valid());
        }
        if (mb > 0.f) {
            res_b.displacement = -d * mb * multiplier;
            assert(res_b.displacement.is_valid());
        }
    }

    pa = pa + res_a.displacement;
    pb = pb + res_b.displacement;

    auto raCn = ra.cross(n);
    auto rbCn = rb.cross(n);

    auto normal_mass = invert(ma + mb + raCn.dot(ia * raCn) + rbCn.dot(ib * rbCn));
    auto tangent_mass = std::array<float, 2> {};

    for (auto i = 0; i < 2; i++) {
        auto raCt = ra.cross(tangent_vectors[i]);
        auto rbCt = rb.cross(tangent_vectors[i]);
        tangent_mass[i] = invert(ma + mb + raCt.dot(ia * raCt) + rbCt.dot(ib * rbCt));
    }

    // Velocity at contact point
    auto dv = - (va + wa.cross(ra)) + (vb + wb.cross(rb));

    auto v = dv.dot(n);
    assert(relative_eq(n.length(), 1.0f));
    if (v > 0) return { res_a, res_b };

    if (-v < 0.01) elasticity = 0.0; // Let bodies come to rest
    //if (-v > 2.0) elasticity /= -v / 5.0; // Hacky non-linear elasticity

    // Impulse
    auto _jn = -(1.0f + elasticity) * v * normal_mass;
    auto jn = n * _jn;

    res_a.linear_impulse = -jn * ma;
    res_a.angular_impulse = ia * -ra.cross(jn);

    res_b.linear_impulse = jn * mb;
    res_b.angular_impulse = ib * rb.cross(jn);

    // Friction
    for (auto i = 0; i < 2; i++) {
        auto max_lambda = std::abs(_jn * friction);
        auto lambda = -dv.dot(tangent_vectors[i]) * tangent_mass[i];
        lambda = clamp(lambda, -max_lambda, max_lambda);
        auto jt = tangent_vectors[i] * lambda;

        res_a.linear_impulse = res_a.linear_impulse - jt * ma;
        res_a.angular_impulse = res_a.angular_impulse - ia * ra.cross(jt);

        res_b.linear_impulse = res_b.linear_impulse + jt * mb;
        res_b.angular_impulse = res_b.angular_impulse + ib * rb.cross(jt);
    }

    auto w = wa.dot(n);
    res_a.angular_impulse =
            res_a.angular_impulse +
            -wa +
            n * wa.dot(n) * 0.95 +
            tangent_vectors[0] * wa.dot(tangent_vectors[0]) * 0.99 +
            tangent_vectors[1] * wa.dot(tangent_vectors[1]) * 0.99;

    return { res_a, res_b };
}

#endif
