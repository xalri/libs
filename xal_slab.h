#ifndef XAL_LIBS_SLAB_HPP
#define XAL_LIBS_SLAB_HPP

#include <vector>
#include <stdexcept>
#include <cassert>
#include <optional>

#include "third_party/iterator_tpl.h"

/// Pre-allocated storage for uniformly sized types
template<class T>
struct slab {
    struct Entry {
        std::optional<T> value {};
        size_t next_free = 0;

        // marker for overloaded constructor
        inline static int vacant = 0;

        explicit Entry(T value): value(value) {}
        Entry(decltype(vacant) _, size_t next_free): next_free(next_free) {}

        Entry(Entry &&rhs) {
            std::swap(value, rhs.value);
            std::swap(next_free, rhs.next_free);
        }

        Entry& operator=(Entry &&rhs) {
            std::swap(value, rhs.value);
            std::swap(next_free, rhs.next_free);
            return *this;
        }

        Entry(Entry const& rhs) = delete;
        Entry& operator=(Entry const& rhs) = delete;

        ~Entry() = default;

        inline bool occupied() const { return value.has_value(); }
    };

    std::vector<Entry> entries {};
    size_t len = 0;
    size_t next = 0;

    size_t insert(T&& value) {
        auto key = next;
        insert_at(key, std::move(value));
        return key;
    }

    size_t reserve() {
        auto key = next;
        next = key == entries.size() ? key + 1 : entries[key].next_free;
        return key;
    }

    void insert_at(size_t key, T&& value) {
        assert(!contains(key));

        while (key > entries.size()) {
            entries.emplace_back(Entry::vacant, next);
            next == entries.size() - 1;
        }

        len += 1;

        if (key == entries.size()) {
            entries.emplace_back(value);
            next = key + 1;
        } else {
            auto prev = Entry(value);
            std::swap(entries[key], prev);
            assert(!prev.occupied());
            next = prev.next_free;
        }
    }

    T&& remove(size_t key) {
        auto prev = Entry(Entry::vacant, next);
        std::swap(prev, entries[key]);

        assert(prev.occupied());
        len -= 1;
        next = key;
        return std::move(*prev.value);
    }

    bool contains(size_t idx) const {
        return idx < entries.size() && entries[idx].occupied();
    }

    T const& operator[](size_t idx) const {
        return *entries[idx].value;
    }

    T& operator[](size_t idx) {
        return *entries[idx].value;
    }

    struct it_state {
        uint64_t pos;
        inline void next(const slab* ref) {
            pos++;
            while(pos < ref->entries.size() && !ref->entries[pos].value.has_value()) pos++;
        }
        inline void begin(const slab* _ref) { pos = 0ull; }
        inline void end(const slab* ref) { pos = ref->entries.size(); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline T const& get(const slab* ref) const { return (*ref)[pos]; }
        inline T& get(slab* ref) const { return (*ref)[pos]; }
    };
    SETUP_ITERATORS(slab, T&, it_state);
};

#endif