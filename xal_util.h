#ifndef XAL_LIBS_UTIL_HPP
#define XAL_LIBS_UTIL_HPP

#include <stdexcept>
#include <algorithm>
#include <random>
#include <thread>
#include <random>
#include <string>
#include "xal_time.h"

#ifdef _WIN32
    #include <direct.h>
    #define getcwd _getcwd
#else
    #include <unistd.h>
#endif

/// Get the current working directory
inline std::string cwd() {
    char buffer[2048];
    char *result = getcwd(buffer, sizeof(buffer));
    auto str = std::string();
    if (result) str = result;
	std::replace(str.begin(), str.end(), '\\', '/');
    return str;
}

/// An exception for not-yet-implemented code.
class unimplemented: public std::logic_error {
public:
    unimplemented(): logic_error("Not yet implemented") {}
};

template<class T> void drop(T) {}

/// Mark a struct as non-movable & non-copyable
struct non_movable {
	non_movable(non_movable const &) = delete;
	non_movable(non_movable &&) = delete;
	non_movable &operator=(non_movable const &) = delete;
	non_movable &operator=(non_movable &&) = delete;

	non_movable() = default;
};

/// Mark a struct as non-copyable
struct non_copyable {
	non_copyable(non_movable const &) = delete;
	non_movable &operator=(non_copyable const &) = delete;

	non_copyable() = default;
};

namespace mem {
template<class T> T&& zero() {
	char tmp[sizeof(T)] = { 0 };
	return std::move(*reinterpret_cast<T*>(tmp));
}
}

// https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring/217605#217605
#include <algorithm>
#include <cctype>
#include <locale>

/// Trim from start (in place)
inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

/// Trim from end (in place)
inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

/// Trim from both ends (in place)
inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}

#endif
