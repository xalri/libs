#ifndef XAL_LIBS_PROF_HPP
#define XAL_LIBS_PROF_HPP

#include <ostream>
#include <limits>
#include <memory>
#include <map>
#include <sstream>
#include "xal_buffer.h"
#include "xal_time.h"

/// Statistical measures for some data.
template<class T> struct Stats {
    bool empty = true;
    T min = std::numeric_limits<T>::max();
    T max = -std::numeric_limits<T>::max();
    T avr = 0;

    template <class iter> Stats(iter it, iter end) {
        auto n = 0;
        while (it != end) {
            n += 1;
            empty = false;
            auto value = *(it++);
            avr += value;
            if (value < min) min = value;
            if (value > max) max = value;
        }
        avr /= (T)n;
    }

    friend std::ostream &operator<<(std::ostream &os, const Stats &stats) {
        if (stats.empty) {
            os << "-";
        } else {
            os.precision(6);
            os << std::fixed;
            os << "min: " << stats.min << " max: " << stats.max << " avr: " << stats.avr;
        }
        return os;
    }
};

inline unsigned long BUFFER_SIZE = 1024;

/// Tracks the time taken to run multiple instances of a task
class Profile {
    std::shared_ptr<Buffer<double, BUFFER_SIZE>> buffer = std::make_shared<Buffer<double, BUFFER_SIZE>>();
    std::string name = "";
public:

    struct Block {
        std::shared_ptr<Buffer<double, BUFFER_SIZE>> buffer;
        uint64_t start;
        bool done = false;

        Block(const Block&) = delete;
        Block& operator=(const Block&) = delete;
        Block& operator=(Block&& other) {
            end();
            other.done = true;
            start = other.start;
            buffer = other.buffer;
            done = false;
            return *this;
        }
        Block(Block&&) noexcept = default;

        double elapsed() const { return ns_to_ms(now() - start); }

        void end() { if (!done) { done = true; buffer->add(elapsed()); } }

        ~Block() {
			if (buffer != nullptr) end();
		}
    };

    explicit Profile(std::string const& name): name(std::move(name)) {}

    /// Start a new block. Drop the returned value at the end of the task to
    /// finish timing.
    Block start_block() {
        return Block{buffer, now()};
    }

    /// Get the min, max, and average duration in milliseconds.
    Stats<double> stats() const {
        auto stats = Stats<double>(buffer->begin(), buffer->end());
        return stats;
    }

    friend std::ostream &operator<<(std::ostream &os, Profile const& profile) {
        os << profile.stats() << " (ms) | " << profile.name;
        return os;
    }

    std::string to_string() const {
        auto ss = std::stringstream {};
        ss << *this;
        return ss.str();
    }
};

/// Track the time taken by multiple tasks identified by name.
struct SplitProfile {
    std::map<std::string, Profile> profiles{};

    Profile& profile(std::string const& name) {
        auto it = profiles.find(name);
        if (it == profiles.end()) it = profiles.emplace(name, Profile(name)).first;
        return it->second;
    }

    Profile::Block start_block(std::string const& name) {
        return profile(name).start_block();
    }

    friend std::ostream &operator<<(std::ostream &os, SplitProfile const& split) {
        for(auto p : split.profiles) os << p.second << std::endl;
        return os;
    }

    std::string to_string() const {
        auto ss = std::stringstream {};
        ss << *this;
        return ss.str();
    }
};

template<class Dummy> struct xal_prof_statics_t { static SplitProfile global_profile; };
template<class Dummy>
SplitProfile xal_prof_statics_t<Dummy>::global_profile = {};

using xal_prof_statics = xal_prof_statics_t<void>;

Profile::Block prof(std::string const& name) {
    return xal_prof_statics::global_profile.start_block(name);
};

#endif
