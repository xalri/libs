#ifndef XAL_LIBS_TIME_HPP
#define XAL_LIBS_TIME_HPP

#include <chrono>
#include <functional>
#include <utility>
#include <map>
#include <memory>
#include <mutex>
#include <iostream>
#include <string>

/// Get the current time in nanoseconds, relative to some epoch consistent within the program
inline uint64_t now() {
    auto time = std::chrono::high_resolution_clock::now();
    auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(time.time_since_epoch());
    return static_cast<uint64_t>(ns.count());
}

inline double ns_to_ms(uint64_t ns) { return (double) ns / 1'000'000.0; }
inline double ns_to_seconds(uint64_t ns) { return (double) ns / 1'000'000'000.0; }
inline uint64_t ms_to_ns(double ms) { return (uint64_t)(ms * 1'000'000.0); }
inline uint64_t seconds_to_ns(double ms) { return (uint64_t)(ms * 1'000'000'000.0); }

/// An interval timer.
class StepTimer {
public:
    uint64_t step;
    uint64_t time = now();
    uint64_t rollover = 0ull;

    /// Create a timer with the given interval in nanoseconds
    explicit StepTimer(uint64_t step = 1000000000ull / 30ull): step(step) {}

    /// Run fn for each tick since the last call.
    void tick(std::function<void()> fn) {
        auto diff = now() - time + rollover;
        time = now();

        while (diff >= step) {
            diff -= step;
            fn();
        }
        rollover = diff;
    }
};

#endif
