#ifndef XAL_LIBS_NAV_HPP
#define XAL_LIBS_NAV_HPP

#include <vector>
#include <algorithm>
#include <queue>
#include <optional>
#include "xal_math.h"

/// A navigable graph >_>
struct NavGraph {
    struct Node {
        v3 position;
        std::vector<size_t> connected {}; // TODO: smallvec
    };
    std::vector<Node> nodes;

    /// Build a navigation graph from a mesh, given a list of triangle indices and a method for
    /// getting the position of a node given an index.
    /// Must be fully connected
    static NavGraph build(std::vector<uint16_t> const& idx, std::vector<v3> const& points) {
        auto nodes = std::vector<Node> {};
        for (auto &p : points) nodes.push_back({ p });

        auto i = 0u;
        while (i < idx.size()) {
            auto a = (size_t) idx[i];
            auto b = (size_t) idx[i + 1];
            auto c = (size_t) idx[i + 2];

            nodes[a].connected.push_back(b);
            nodes[a].connected.push_back(c);
            nodes[b].connected.push_back(a);
            nodes[b].connected.push_back(c);
            nodes[c].connected.push_back(a);
            nodes[c].connected.push_back(b);

            i += 3;
        }

        for (auto& n : nodes) {
            // Sort & deduplicate connected points
            std::sort(n.connected.begin(), n.connected.end());
            n.connected.erase(std::unique(n.connected.begin(), n.connected.end()), n.connected.end());
        }
        return NavGraph { nodes };
    }

    /// Get the index of the closest node to some point.
    size_t closest_node(v3 point, float min_sq = 0.0f) const {
        assert(!nodes.empty());

        auto node = 0u;
        auto dist_sq = (point - nodes[0].position).length_sq();
        for (auto i = 1u; i < nodes.size(); i++) {
            auto _dist_sq = (point - nodes[i].position).length_sq();
            if (_dist_sq > min_sq && _dist_sq < dist_sq) {
                node = i;
                dist_sq = _dist_sq;
            }
        }

        return node;
    }

    /// Traverse the graph to find a path from one point to another.
    std::vector<v3> find_path(v3 from, v3 to) const {
        assert(!nodes.empty());
        std::vector<v3> path{};
        path.push_back(to);

        // Nodes which have been evaluated
        std::vector<size_t> closed {};
        // Currently discovered but not-yet evaluated nodes
        // (heap order is maintained with std::push_heap & std::pop_heap)
        std::vector<size_t> open {};
        // The best-known previous node for getting from the start to each node.
        std::vector<std::optional<size_t>> parent(nodes.size(), std::nullopt);
        // The cost of getting from the start to each node.
        std::vector<float> g_score(nodes.size(), std::numeric_limits<float>::max());
        // The total cost of getting from the start node to each node via that node.
        std::vector<float> f_score(nodes.size(), std::numeric_limits<float>::max());

        // Comparator for heap functions based on f score.
        auto cmp_f = [&](size_t a, size_t b) {
            return f_score[a] > f_score[b];
            // cmp expects less than
            // priority queue gives highest
        };

        // Get the value in the open set with the smallest f score.
        auto pop_open = [&]() -> size_t {
            std::pop_heap(open.begin(), open.end(), cmp_f);
            auto n = open.back();
            open.pop_back();
            return n;
        };

        // Add a value to the open set.
        auto push_open = [&](size_t n) {
            open.push_back(n);
            std::push_heap(open.begin(), open.end(), cmp_f);
        };

        // Check if a node is in the open set
        auto is_open = [&](size_t n) -> bool {
            for(auto& b : open) if (b == n) return true;
            return false;
        };

        // Check if a node is in the closed set
        auto is_closed = [&](size_t n) -> bool {
            for(auto& b : closed) if (b == n) return true;
            return false;
        };

        auto start_node_a = closest_node(from);
        auto node_a_dist = (nodes[start_node_a].position - from).length_sq();

        /*
        auto start_node_b = closest_node(from, node_a_dist);
        auto node_b_dist = (nodes[start_node_b].position - from).length_sq();

        auto start_node_c = closest_node(from, node_b_dist);
        auto node_c_dist = (nodes[start_node_c].position - from).length_sq();
         */

        auto end_node = closest_node(to);

        // Some applicable heuristic of cost between nodes, in this case euclidean distance.
        auto h = [&](size_t from, size_t to) {
            return (nodes[from].position - nodes[to].position).length();
        };

        open.push_back(start_node_a);
        g_score[start_node_a] = node_a_dist;
        f_score[start_node_a] = h(start_node_a, end_node);

        /*
        open.push_back(start_node_b);
        g_score[start_node_b] = node_b_dist;
        f_score[start_node_b] = h(start_node_b, end_node);

        open.push_back(start_node_c);
        g_score[start_node_c] = node_b_dist;
        f_score[start_node_c] = h(start_node_c, end_node);
         */

        while(!open.empty()) {
            auto current = pop_open();

            if (current == end_node) {
                while (parent[current].has_value()) {
                    current = *parent[current];
                    path.push_back(nodes[current].position);
                }
                break;
            }

            closed.push_back(current);

            for (auto n : nodes[current].connected) {
                if (is_closed(n)) continue;

                auto g = g_score[current] + h(current, n);

                if (!is_open(n)) push_open(n);
                else if (g >= g_score[n]) continue;

                parent[n] = { current };
                g_score[n] = g;
                f_score[n] = g_score[n] + h(n, end_node);
            }
        }

        if (closed.size() == nodes.size()) {
            // things are probably broken :(
            assert(false);
        }

        // TODO: string pulling within nav volume?

        path.push_back(from);
        // TODO: reverse path
        return path;
    }
};

#endif
