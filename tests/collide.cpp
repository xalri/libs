#include "catch.hpp"
#include "xal_collide.h"

TEST_CASE( "Circle-Circle collision", "[collide]" ) {
    REQUIRE(intersect(
            Sphere { 1, v3{0}, v3{0} },
            Sphere { 1, v3{1, 0, 0}, v3{0} }
    ) == Intersection{ v3{1, 0, 0}, { 1 }, { 0 } });

    REQUIRE(intersect(
            Sphere { 9, v3{10, 10, 0}, v3{0, -20, 0} },
            Sphere { 9, v3{-10, -10, 0}, v3{0, 20, 0} }
    ) == Intersection{ v3{}, {}, {} });

    REQUIRE(intersect(
            Sphere { 5, v3{}, v3{} },
            Sphere { 5, v3{20, 0, 0}, v3{-20, 0, 0} }
    ) == Intersection{ v3{1, 0, 0}.normalize(), {}, { 0.5 } });

    REQUIRE(intersect(
            Sphere { 5, v3{10, 10, 0}, v3{-10, -10, 0} },
            Sphere { 5, v3{-10, -10, 0}, v3{10, 10, 0} }
    ) == Intersection{ v3{-1, -1, 0}.normalize(), {}, { 1 - (10 / v2{20,20}.length()) } });
}

TEST_CASE( "Circle-Triangle collision", "[collide]" ) {
    REQUIRE(intersect(
            Sphere { 1, v3{4, 4, 0}, v3{-10, 0, 0} },
            Triangle { {0, 0, 0}, {2, 0, 0}, {2, 5, 0} }
    ) == Intersection{ v3{-1, 0, 0}, {}, { 0.1 } });
}