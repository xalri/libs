#include "catch.hpp"
#include "xal_math.h"

TEST_CASE( "Rotors works", "[collide]" ) {
    REQUIRE(
        r3::make(v3{1, 0, 0}, v3{0, 1, 0}).rotate(v3{ 1, 0, 0 }) ==
        v3{0, 1, 0}
    );
}

TEST_CASE( "Rotor composition works", "[collide]" ) {
    REQUIRE(
            r3::make(v3{1, 0, 0}, v3{0, 1, 0}) ==
            r3::make(v3{0, -1, 0}, v3{1, 0, 0})
    );

    auto norm = v3{0, 1, 0}.cross(v3{1, 0, 0});
    REQUIRE(
            r3::from_angle(M_PI / 2, norm) * r3::from_angle(M_PI / 2, norm) ==
            r3::from_angle(M_PI, norm)
    );
}
